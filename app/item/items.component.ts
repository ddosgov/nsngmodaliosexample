import { Component, OnInit, ViewContainerRef } from "@angular/core";

import { Item } from "./item";
import { AuthComponent } from '../auth.component';
import { ItemService } from "./item.service";
import { ModalDialogService } from 'nativescript-angular/directives/dialogs';

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
})
export class ItemsComponent implements OnInit {
    items: Item[];

    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class.
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    constructor(private itemService: ItemService,  private modalService: ModalDialogService,  private viewContainerRef: ViewContainerRef) { }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    clock() {
        this.modalService.showModal(AuthComponent, {
            fullscreen: true,
            viewContainerRef: this.viewContainerRef
        });
    }
}