import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from 'nativescript-angular/directives/dialogs';

@Component({
    selector: 'ns-auth',
    moduleId: module.id,
    templateUrl: './auth.component.html',
})
export class AuthComponent implements OnInit {
    constructor(
        private params: ModalDialogParams
    ) { }

    ngOnInit(): void {
    }

    close(res: string) {
        this.params.closeCallback(res);
    }
}
